import unittest
import requests
from bs4 import BeautifulSoup

from Main import get_daily_leaders_stats, get_daily_leaders


class TestSum(unittest.TestCase):

    def test_call_endpoint(self):
        response = requests.get("http://www.espn.com/nba/dailyleaders")
        assert response.status_code == 200
        soup = BeautifulSoup(response.content, "html.parser")
        assert len(get_daily_leaders_stats(soup)) == 5
        assert len(get_daily_leaders(soup)) == 5

    def test_call_endpoint_api(self):
        params = {'page': '0',
                  'per_page': '50',
                  'search': 'James'}
        headers = {'X-RapidAPI-Key': '0a59c266c0msh72f9a47c8dcfafap1a0827jsnbed76290f835',
                   'X-RapidAPI-Host': 'free-nba.p.rapidapi.com'}

        response = requests.get("https://free-nba.p.rapidapi.com/players", params=params, headers=headers)
        assert response.status_code == 200


if __name__ == '__main__':
    unittest.main()
