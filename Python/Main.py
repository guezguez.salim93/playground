#create init file for mysql
#fix app container
from bs4 import BeautifulSoup
import requests
import save_informations


def create_soup():
    req = requests.get("http://www.espn.com/nba/dailyleaders")
    return BeautifulSoup(req.content, "html.parser")


def get_daily_leaders_stats(soup):
    daily_leaders_stats = []
    daily_leaders_points = [tag.find_next_sibling("td").text for tag in soup.find_all('td', text='PTS')]
    daily_leaders_assists = [tag.find_next_sibling("td").text for tag in soup.find_all('td', text='AST')]
    daily_leaders_rebounds = [tag.find_next_sibling("td").text for tag in soup.find_all('td', text='REB')]
    daily_leaders_blocks = [tag.find_next_sibling("td").text for tag in soup.find_all('td', text='BLK')]
    for i in range(5):
        daily_leaders_stats.append({
            'PTS': daily_leaders_points[i],
            'AST': daily_leaders_assists[i],
            'REB': daily_leaders_rebounds[i],
            'BLK': daily_leaders_blocks[i]
        })
    return daily_leaders_stats


def get_daily_leaders(soup):
    return [tag.contents[0] for tag in soup.findAll('strong') if len(tag.contents[0]) > 3]


def get_lastname(name):
    return name[3:]


def get_player_information(player_name):
    params = {'page': '0',
              'per_page': '50',
              'search': player_name[3:]}
    headers = {'X-RapidAPI-Key': '0a59c266c0msh72f9a47c8dcfafap1a0827jsnbed76290f835',
               'X-RapidAPI-Host': 'free-nba.p.rapidapi.com'}

    response = requests.get("https://free-nba.p.rapidapi.com/players", params=params, headers=headers)
    for player in response.json()['data']:
        if player['first_name'][0] == player_name[0]:
            return {'fullname': player['first_name'] + ' ' + player['last_name'],
                    'team': player['team']['full_name'],
                    'conference': player['team']['conference']}
    return None


def get_daily_leaders_information():
    soup = create_soup()
    daily_leaders_stats = get_daily_leaders_stats(soup)
    daily_leaders = get_daily_leaders(soup)
    result = []
    for i in range(5):
        player_information = get_player_information(daily_leaders[i])
        try:
            player_information.update(daily_leaders_stats[i])
        except AttributeError:
            player_information = 'player not found'
        result.append(player_information)
    return result


print(get_daily_leaders_information())
save_informations.save_data(get_daily_leaders_information())
