import mysql
from mysql.connector import Error
from datetime import date



def save_data(daily_information):
    try:
        connection = mysql.connector.connect(host='db',
                                             port=3306,
                                             database='mydb',
                                             user='root',
                                             password='password')
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record)
            query = "INSERT INTO players (dates, fullname, team, conference, PTS, AST, REB, BLK) VALUES (%s,%s,%s,%s,%s,%s,%s,%s) "
            daily_information = add_date(daily_information)
            for player in daily_information:
                cursor.execute(query, tuple(player.values()))
            connection.commit()

    except Error as e:
        print("Error while connecting to MySQL", e)


def add_date(daily_information):
    today = date.today()
    result = [{"dates": today}, {"dates": today}, {"dates": today}, {"dates": today}, {"dates": today}]
    for i in range(5):
        result[i].update(daily_information[i])
    return result
